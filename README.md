# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Pepega project
* Key functions (add/delete)
    1. 論壇
    
* Other functions (add/delete)
    1. Post
    2. Comment
    3. Feedback

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://pepega-project.firebaseapp.com/index.html

# Components Description : 
1. Background : Ricardo Milos 的熱舞
2. Welcome : 歡迎介面，如果按下面的 "Ok, Im leaving" 的話，系統會跳出訊息來挽留你。
3. Userpage : 個人頁面，包含了非常精緻的 512*512 個人大頭貼，還有自己的電子信箱和網路暱稱。（附上Pepega的meme解釋頁面）。
4. Forum : 點擊後可以進入論壇的畫面（登入後才能觀看貼文）。
5. Meme : 放了一些有趣的圖。
6. Contact us : 可以給 Pepega 公司團隊一些回饋 （必須要登入），畢竟不是會員我們懶得理你的回饋。
...

# Other Functions Description(1~10%) : 
1. 登入登出 : 左上角的 Account 點開可以登入登出，Login 方式可用電子信箱註冊，還有 Google 帳號登入。
2. Chrome 通知 : 進入網頁後會詢問是否接受通知，接受後會彈出通知，最好不要點開它，因為會幫你點開色情網站。
<img src="https://i.imgur.com/anoTuTL.png"></img>
3. 主頁面漂亮的CSS動畫 : 套模板也是一件累人的事。
4. Forum 的 Comment 功能要先點標題進去才行。 
...

## Security Report (Optional)
1. 使用者如果不登入就無法使用 Forum 的貼文 (Post) 以及留言 (Comment) ，還有提供回饋 (Feedback) 的功能。
2. 因為資料庫的讀寫必須登入，規則如下。
3. <img src="https://i.imgur.com/5OPvRxq.png"></img>
