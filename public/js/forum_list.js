var user_email = '';
var user_image = '';

function init() {

    document.getElementById("comment-container").style.display = "none";
    document.getElementById("return_btn").onclick = function () {
        window.location = "forum_list.html";
    };
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(function() {
    // Existing and future Auth states are now persisted in the current
    // session only. Closing the window would clear any existing state even
    // if a user forgets to sign out.
    // ...
    // New sign-in will be persisted with session persistence.
    return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    });

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            if (user.displayName) {
                user_name = user.displayName;
            } else {
                user_name = "[edit below]";
            }
            if (user.photoURL) {
                user_image = user.photoURL;

            } else {
                user_image = "img/pepega.jpg";
            }  
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span >"
                + "<span class='dropdown-item' id='logout-btn'>Logout</span>";

            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            logout_btn = document.getElementById('logout-btn');
            logout_btn.addEventListener('click',function()
            {
                firebase.auth().signOut().then(function()
                {
                    alert('success');
                }).catch(function()
                {
                    alert('failed');
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
 
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_title = document.getElementById('new-post-title');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var list = firebase.database().ref('com_list').push().set({
                email : user_email,
                avatar : user_image,
                title : post_title.value,
                postdata : post_txt.value
            });
            
            post_txt.value='';
            post_title.value='';
        }
    });

    // The html code for post
    //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_before_username_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'><a href='#' id='";
    var str_before_username_11 = "'>【";
    var str_before_username_2 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='"
    var str_before_username_22 = "' alt='' class='mr-2 rounded' style='height:100px; width:100px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
        ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
        ///         2. Join all post in list to html in once
        ///         4. Add listener for update the new post
        ///         5. Push new post's html to a list
        ///         6. Re-join all post in list to html when update
        ///
        ///         Hint: When history post count is less then new post count, update the new and refresh html
           
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username_1
                    + childSnapshot.key + str_before_username_11
                    + childData.title + "】 " + "</a>"
                    + str_before_username_2 + childData.avatar + str_before_username_22
                    + "來自" + childData.email + "的貼文" + "</strong>" + "貼文內容：" + "</br>" + childData.postdata
                    + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            setLink("all");

            postsRef.on('child_added', function (data) {    // show added comment anytime
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    console.log(childData);
                    total_post[total_post.length] = str_before_username_1
                        + childData.key + str_before_username_11
                        + childData.title + "】 " + "</a>"
                        + str_before_username_2 + childData.avatar + str_before_username_22
                        + "來自" + childData.email + "的貼文" + "</strong>" + "貼文內容：" + "</br>" + childData.postdata
                        + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    setLink(childData.key);
                }
            });
        })
    .catch(e => console.log(e.message));

    if (!('Notification' in window)) {
        console.log('本瀏覽器不支援推播通知');
    }
    
    
    if ("Notification" in window)
    {
        let ask = Notification.requestPermission();
        ask.then(permission=> {
            if(permission == "granted")
            {
                let msg = new Notification("Ricardo Milos wants to know your location", {
                    body: "OMEGALUL",
                    icon: "img/omegalul.png"
                });
                msg.addEventListener("click", event=> {
                    alert("You are GAY");
                    window.open('https://www.pornhub.com/gayporn');
                });
            }
        });
    }
}

window.onload = function () {
    init();
};

function setLink(id)
{
    if (id == "all") {
        // Iterate comment list
        var query = firebase.database().ref("com_list").orderByKey();
        query.once("value")
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    setLink(childSnapshot.key);
                });
            });

    } else {
        // Add listenr for specific id
        var domObj = document.getElementById(id);
        domObj.addEventListener("click", function () {
            showPost(id);
        });
    }
}

function showPost(id)
{
    // Change post div
    document.getElementById("post-container").style.display = "none";
    document.getElementById("comment-container").style.display = "block";

    // Update database when comment
    var btn = document.getElementById("comment_btn");
    var txt = document.getElementById("comment-content");
    btn.addEventListener("click", function () {
        if (txt.value != "") {
            var commentRef = firebase.database().ref('com_list/' + id).child("comments").push();
            var user = firebase.auth().currentUser;
            if (user) {
                user_email = user.email;
                if (user.displayName) {
                    user_name = user.displayName;
                } else {
                    user_name = "[edit below]";
                }
                if (user.photoURL) {
                    user_image = user.photoURL;

                } else {
                    user_image = "img/pepega.jpg";
                }
            }
            commentRef.set(
            {
                email: user_email,
                avatar: user_image,
                postdata: txt.value
            });
            txt.value = "";
        }
    });

    // Show only that post and its comments
    var postsRef = firebase.database().ref("com_list/" + id);
    var total_post = [];

    // show post
    var str_before_username_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'>[樓主"
    var str_before_username_2 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='";
    var str_before_username_8 = "' alt='' class='mr-2 rounded' style='height:100px; width:100px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    postsRef.once("value", function (snapshot) {
        var data = snapshot.val();
        total_post[total_post.length] = str_before_username_1
            + "] " + data.title
            + str_before_username_2 + data.avatar + str_before_username_8
            + data.email + "</strong>" + data.postdata
            + str_after_content;
    });


    // show comments
    var commentRef = postsRef.child("comments");

    var first_count = 0;
    var second_count = 0;
    var post_number = 0;

    var str_before_username_3 = "<div class='my-3 p-3 bg-white rounded box-shadow'>"
        + "<h6 class='border-bottom border-gray pb-2 mb-0'>#"
    var str_before_username_4 = "</h6>"
        + "<div class='media text-muted pt-3'>"
        + "<img src='";
    var str_before_username_5 = "' alt='' class='mr-2 rounded' style='height:100px; width:100px;'>"
        + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
        + "<strong class='d-block text-gray-dark'>";
    var str_after_content_2 = "</p></div></div>\n";

    commentRef.once('value')      // show comments that exist for once
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                post_number += 1;
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username_3
                    + post_number + str_before_username_4 + childData.avatar + str_before_username_5
                    + childData.email + "</strong>" + childData.postdata
                    + str_after_content_2;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            commentRef.on('child_added', function (data) {    // show added comment anytime
                second_count += 1;
                if (second_count > first_count) {
                    post_number += 1;
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username_3
                        + post_number + str_before_username_4 + childData.avatar + str_before_username_5
                        + childData.email + "</strong>" + childData.postdata
                        + str_after_content_2;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}