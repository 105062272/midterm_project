function init() {
    var user_email = '';
    var user_image = '';
    var user_name ;

    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(function() {
    // Existing and future Auth states are now persisted in the current
    // session only. Closing the window would clear any existing state even
    // if a user forgets to sign out.
    // ...
    // New sign-in will be persisted with session persistence.
    return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    });

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            if (user.displayName) {
                user_name = user.displayName;
            } else {
                user_name = "[edit below]";
            }
            if (user.photoURL) {
                user_image = user.photoURL;

            } else {
                user_image = "img/pepega.jpg";
            }  
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span >"
                + "<span class='dropdown-item' id='logout-btn'>Logout</span>";

            var pageContent = document.getElementById("userpage_content");
            pageContent.innerHTML = "<img src='" + user_image + "' alt='' class='mr-2 rounded'"
                + " style='height:512px; width:512px;' >" + "<br>"
                + "<br>" + "<p class='media-body pb-3 mb-0 medium lh-125 border-bottom border-gray'>"
                + "<b class='d-block text-gray-dark'>Email: </b>" + user_email
                + "<b class='d-block text-gray-dark'>Nickname: </b>" + user_name + "</p>";
            
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            logout_btn = document.getElementById('logout-btn');
            logout_btn.addEventListener('click',function()
            {
                firebase.auth().signOut().then(function()
                {
                    alert('success');
                }).catch(function()
                {
                    alert('failed');
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    document.getElementById("confirm_btn").onclick = function () {
        // update user info
        var currUser = firebase.auth().currentUser;
        var nameStr = document.getElementById("name_content").value;
        var photoURLStr = document.getElementById("photoURL_content").value;
        var profile =
        {
            displayName: nameStr,
            photoURL: photoURLStr
        };
        if (nameStr && photoURLStr) {
            currUser.updateProfile(profile);
        }
        
        nameStr = "";
        photoURLStr = "";

        // redirect
        window.location = "index.html";
    };

    if (!('Notification' in window)) {
        console.log('本瀏覽器不支援推播通知');
    }
    
    
    if ("Notification" in window)
    {
        let ask = Notification.requestPermission();
        ask.then(permission=> {
            if(permission == "granted")
            {
                let msg = new Notification("Ricardo Milos wants to know your location", {
                    body: "OMEGALUL",
                    icon: "img/omegalul.png"
                });
                msg.addEventListener("click", event=> {
                    alert("You are GAY");
                    window.open('https://www.pornhub.com/gayporn');
                });
            }
        });
    }
}

window.onload = function () {
    init();
};